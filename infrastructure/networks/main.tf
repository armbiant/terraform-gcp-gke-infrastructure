locals {
  network_name                   = "my-net-2"
  subnet_name                    = "my-subnet-2"
  cluster_master_ip_cidr_range   = "192.168.0.0/20"
  cluster_pods_ip_cidr_range     = "10.4.0.0/14"
  cluster_services_ip_cidr_range = "10.0.32.0/20"
}

resource "google_compute_network" "vpc" {
  name                            = local.network_name
  project                         = var.project_id
  auto_create_subnetworks         = false
  routing_mode                    = "REGIONAL"
  delete_default_routes_on_create = false
}

resource "google_compute_subnetwork" "subnet" {
  name          = local.subnet_name
  ip_cidr_range = local.cluster_master_ip_cidr_range
  project       = var.project_id
  region        = var.region
  network       = local.network_name
  //purpose                    = "PRIVATE"
  private_ip_google_access   = true
  private_ipv6_google_access = "DISABLE_GOOGLE_ACCESS"
  //stack_type                 = "IPV4_ONLY"

  secondary_ip_range {
    ip_cidr_range = local.cluster_pods_ip_cidr_range
    range_name    = "my-pods"
  }

  secondary_ip_range {
    ip_cidr_range = local.cluster_services_ip_cidr_range
    range_name    = "my-services"
  }

}

resource "google_compute_firewall" "fw" {
  allow {
    ports    = ["22"]
    protocol = "tcp"
  }

  allow {
    ports    = ["3389"]
    protocol = "tcp"
  }

  direction     = "INGRESS"
  name          = "${local.network_name}-allow-iap"
  network       = local.network_name
  priority      = 1000
  project       = var.project_id
  source_ranges = ["35.235.240.0/20"]
  target_tags   = ["iap-demo"]
}

resource "google_compute_router" "router" {
  name    = "${local.network_name}-router"
  region  = var.region
  network = local.network_name
}

resource "google_compute_router_nat" "nat_router" {
  name                               = "${local.subnet_name}-nat-router"
  router                             = google_compute_router.router.name
  region                             = var.region
  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"

  log_config {
    enable = true
    filter = "ERRORS_ONLY"
  }
}
