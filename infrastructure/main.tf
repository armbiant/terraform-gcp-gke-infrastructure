terraform {
  required_providers {
    google = {
        source = "hashicorp/google"
        version = "3.51.0"
      }
  }
  
  backend "local" {
    path = "./tfstate/terraform.tfstate"
  }
}

provider "google" {
  project = var.project_id
  region  = var.region
  zone    = var.main_zone
}

module "google_networks" {
  source = "./networks"

  project_id = var.project_id
  region     = var.region
}

module "bastion" {
  source = "./bastion"

  project_id   = var.project_id
  region       = var.region
  zone         = var.main_zone
  bastion_name = "dan"
  network_name = module.google_networks.network.name
  subnet_name  = module.google_networks.subnet.name
}
