locals {
  hostname = format("%s-bastion", var.bastion_name)
}

// Dedicated service account for the Bastion instance.
//resource "google_service_account" "bastion" {
//  account_id   = format("%s-bastion-sa", var.bastion_name)
//  display_name = "GKE Bastion Service Account"
//}

// The user-data script on Bastion instance provisioning.
data "template_file" "startup_script" {
  template = <<-EOF
  sudo apt-get update -y
  sudo apt-get install -y tinyproxy
  EOF
}

// The Bastion host.
resource "google_compute_instance" "bastion" {
  name         = local.hostname
  machine_type = "e2-micro"
  zone         = var.zone
  project      = var.project_id
  tags         = ["iap-demo"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
    auto_delete = true
    mode        = "READ_WRITE"
  }

  shielded_instance_config {
    enable_secure_boot          = true
    enable_vtpm                 = true
    enable_integrity_monitoring = true
  }

  // Install tinyproxy on startup.
  metadata_startup_script = data.template_file.startup_script.rendered

  network_interface {
    network            = var.network_name
    network_ip         = "192.168.0.4"
    subnetwork         = var.subnet_name
    subnetwork_project = var.project_id
  }

  // Allow the instance to be stopped by Terraform when updating configuration.
  allow_stopping_for_update = true

  /* local-exec providers may run before the host has fully initialized.
  However, they are run sequentially in the order they were defined.
  This provider is used to block the subsequent providers until the instance is available. */
  provisioner "local-exec" {
    command = <<EOF
        READY=""
        for i in $(seq 1 30); do
          if gcloud compute ssh ${local.hostname} --project ${var.project_id} --zone ${var.zone} --command uptime; then
            READY="yes"
            break;
          fi
          echo $i
          echo "Waiting for ${local.hostname} to initialize..."
          sleep 10;
        done
        if [[ -z $READY ]]; then
          echo "${local.hostname} failed to start in time."
          echo "Please verify that the instance starts and then re-run `terraform apply`"
          exit 1
        fi
EOF
  }

  // DAN
  service_account {
    email  = "471716218438-compute@developer.gserviceaccount.com"
    scopes = ["cloud-platform", "https://www.googleapis.com/auth/devstorage.read_only", "https://www.googleapis.com/auth/logging.write", "https://www.googleapis.com/auth/monitoring.write", "https://www.googleapis.com/auth/pubsub", "https://www.googleapis.com/auth/service.management.readonly", "https://www.googleapis.com/auth/servicecontrol", "https://www.googleapis.com/auth/trace.append"]
  }

  //  service_account {
  //    email  = google_service_account.bastion.email
  //    scopes = ["cloud-platform"]
  //  }

  scheduling {
    automatic_restart   = true
    on_host_maintenance = "MIGRATE"
    //provisioning_model  = "STANDARD"
  }

}
